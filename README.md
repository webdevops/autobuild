# WebDevOps build environment

[![pipeline status](https://gitlab.com/webdevops/autobuild/badges/master/pipeline.svg)](https://gitlab.com/webdevops/autobuild/commits/master)

We use an [autoscaling GitLab runner on AWS](https://docs.gitlab.com/runner/configuration/autoscale.html#how-concurrent-limit-and-idlecount-generate-the-upper-limit-of-running-machines) and [pipeline schedules](https://docs.gitlab.com/ce/user/project/pipelines/schedules.html) to perform nightly builds.

This automatic build environment basically checks out https://github.com/webdevops/Dockerfile and builds all docker images defined in the master branch.

The AWS runner is sponsored by [Onedrop GmbH & Co. KG](https://1drop.de)
